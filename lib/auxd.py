###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### AUXIILARY 1D WAVE FOR TF/SF EXCITATION      ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import             os
import             sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import             numpy               as  np
from CMatd  import get_coefficients_1d

def get_indeces_boundary(GRIDSPEC):
 def get_indeces_O_1(GRIDSPEC):
  region=[]
  row   =GRIDSPEC[7][0]-1
  for col in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 def get_indeces_O_2(GRIDSPEC):
  region=[]
  col   =GRIDSPEC[7][1]+1
  for row in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 def get_indeces_O_3(GRIDSPEC):
  region=[]
  row   =GRIDSPEC[7][1]+1
  for col in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 def get_indeces_O_4(GRIDSPEC):
  region=[]
  col   =GRIDSPEC[7][0]-1
  for row in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 def get_indeces_I_1(GRIDSPEC):
  region=[]
  row   =GRIDSPEC[7][0]
  for col in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 def get_indeces_I_4(GRIDSPEC):
  region=[]
  col   =GRIDSPEC[7][0]
  for row in range(GRIDSPEC[7][0],GRIDSPEC[7][1]+1):
   region.append([row,col])
  return region
 O_1=get_indeces_O_1(GRIDSPEC)
 O_2=get_indeces_O_2(GRIDSPEC)
 O_3=get_indeces_O_3(GRIDSPEC)
 O_4=get_indeces_O_4(GRIDSPEC)
 I_1=get_indeces_I_1(GRIDSPEC)
 I_4=get_indeces_I_4(GRIDSPEC)
 return [O_1,O_2,O_3,O_4,I_1,I_4]

def get_aux_N(PARAMS,GRIDSPEC):
 angle                 =PARAMS[5][3]*np.pi/180  
 length_side_horizental=GRIDSPEC[7][1]-GRIDSPEC[7][0]+1
 length_side_vertical  =length_side_horizental
 length_visible        =1.25*np.sqrt(np.power(length_side_horizental,2)+np.power(length_side_vertical,2))
 length_visible        =int(np.floor(length_visible)+1)
 length_pml            =int(np.floor(0.25*length_visible                                                         )+1)
 length_buffer         =int(np.floor(0.20*length_visible                                                         )+1)
 length_1d_aux         =length_pml+length_buffer+length_visible+length_buffer+length_pml
 return [length_1d_aux,length_pml,length_buffer,length_visible]

def get_indeces_range_pml(TOTAL,PML):
 range_left =[0, PML-1                       ]
 range_right=[(TOTAL-1)-i for i in range(PML)]
 range_right.sort()
 range_right=[range_right[0],range_right[-1]]
 return [range_left,range_right]

def get_indeces_update_ey(GRIDSPEC):
 indeces=[]
 for i in range(1,GRIDSPEC[2]-1):
  indeces.append(i)
 return indeces

def get_indeces_update_hz(GRIDSPEC):
 indeces=[]
 for i in range(  GRIDSPEC[2]-1):
  indeces.append(i)
 return indeces

def get_indeces_connection(PARAMS,GRIDSPEC,INDXB,AUXN):
 def get_positions(IDX,DISPLACEMENT,ORIGIN,ANGLE,START):
  dot     =lambda x,y: x[0]*y[0]+x[1]*y[1]
  distance=[]
  ap      =[np.cos(ANGLE),np.sin(ANGLE)]
  for index in IDX:
   r      =[(index[1]+DISPLACEMENT[1])-ORIGIN[1],(index[0]+DISPLACEMENT[0])-ORIGIN[0]]
   distance.append(dot(ap,r))
  return [(int(np.floor(d))+START,d-np.floor(d)) for d in distance]
 origin   =[GRIDSPEC[7][0],GRIDSPEC[7][0]]
 angle    =PARAMS[5][3]*np.pi/180
 start    =AUXN[1]+AUXN[2]+1
 O_1_d_ex =get_positions(INDXB[0],[ 1  , 0.5],origin,angle,start)
 O_2_d_hz =get_positions(INDXB[1],[ 0.5, 0.5],origin,angle,start)
 O_2_d_ey =get_positions(INDXB[1],[ 0.5, 0  ],origin,angle,start)
 O_3_d_hz =get_positions(INDXB[2],[ 0.5, 0.5],origin,angle,start)
 O_3_d_ex =get_positions(INDXB[2],[ 0  , 0.5],origin,angle,start)
 O_4_d_ey =get_positions(INDXB[3],[ 0.5, 1  ],origin,angle,start)
 I_1_d_hz =get_positions(INDXB[4],[-0.5, 0.5],origin,angle,start)
 I_4_d_hz =get_positions(INDXB[5],[ 0.5,-0.5],origin,angle,start)
 return [O_1_d_ex,O_2_d_hz,O_2_d_ey,O_3_d_hz,O_3_d_ex,O_4_d_ey,I_1_d_hz,I_4_d_hz]

def get_AUX(PARAMS,GRIDSPEC):
 if  PARAMS[0][0] in [1]:
  AUX  =[[],[],[],[],[],[],[],[[],[],[],[],[],[]],[]]
 else:
  if PARAMS[0][0] in [2]:
   if not PARAMS[1][3]:
    AUX=[[],[],[],[],[],[],[],[[],[],[],[],[],[]],[]]
   else:
    indeces_boundary  =get_indeces_boundary(GRIDSPEC)
    aux_N             =get_aux_N  (PARAMS,  GRIDSPEC)
    pulse_position    =aux_N[1]+int(np.floor(aux_N[2]/2)+1    )
    pml_range         =get_indeces_range_pml(aux_N[0],aux_N[1])
    grid_spec         =[GRIDSPEC[0]        ,
                        GRIDSPEC[1]        ,
                        aux_N[0]           ,
                        aux_N[0]           ,
                        [pulse_position]   ,
                        pml_range          ,
                        []                 ,
                        []                  ]
    Ey                =np.zeros           ((grid_spec[2])  )
    Hz                =np.zeros           ((grid_spec[2])  )
    Cey, Chz          =get_coefficients_1d(PARAMS,grid_spec)
    indeces_update_ey =get_indeces_update_ey(grid_spec)
    indeces_update_hz =get_indeces_update_hz(grid_spec)
    indeces_connection=get_indeces_connection(PARAMS,GRIDSPEC,indeces_boundary,aux_N)
    AUX               =[Ey,Hz,Cey,Chz,indeces_update_hz,indeces_update_ey,grid_spec[4],indeces_boundary,indeces_connection]
 return AUX

def interpolate(X,XNEXT,FRAC):
 return (1-FRAC)*X+FRAC*XNEXT

# AUX[0]: Ey
# AUX[1]: Hz
# AUX[2]: Cey
# AUX[3]: Chz
# AUX[4]: Hz UPDATE INDECES
# AUX[5]: Ey UPDATE INDECES
# AUX[6]: PULSE POSITION
# AUX[7]: TF/SF BOUNDARY   INDECES
# AUX[8]: TF/SF CONNECTION INDECES


