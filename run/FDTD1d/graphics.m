%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz %%%
%%% GRAPHICS                                    %%%
%%% by: OAMEED NOAKOASTEEN                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% VER   : SIMULATION VERSION
% CONFIG: CELL ARRAY CONTAINING: PAUSE

function graphics(VER,CONFIG)
clc
close all

PATH                =fullfile('..','..','simulations',VER)                                 ;

datapath            =fullfile(PATH,'data')                                                 ;
[idx_batch,idx_time]=get_ignore_indeces(PATH)                                              ;
indeces             =rFILENAMES(datapath)                                                  ;

NAME                =fullfile('..','..','simulations',VER,'gif',strcat('data','.gif'))     ;
PAUSE               =CONFIG{1}                                                             ;
h                   =figure                                                                ;
counter             =idx_batch                                                             ;
MAX                 =get_MAXIMUM(datapath,indeces)                                         ;
for index=idx_batch:size(indeces,2)
    display([' Prossessing Batch ',num2str(counter)])
    filename=fullfile(datapath,strcat('batch_'               ,...
                                      num2str(indeces(index)),...
                                      '.h5'                      ))                        ;
    data    =get_data(filename)                                                            ;
    if index==idx_batch
        ar        =0.1                                                                     ;
        fig_config=get(gcf,'position')                                                     ;
        set           (gcf,'position',[      fig_config(1)    ,...
                                             fig_config(2)    ,...
                                             fig_config(3)    ,...
                                       floor(fig_config(3)*ar)    ])                       ;
    end
    if index==idx_batch
        time_start=idx_time                                                                ;
    else
        time_start=1                                                                       ;
    end
    for time=time_start:size(data,2)
        plotter(data(:,time),(index-1)*100+time,MAX)
        [imind,cm]=rgb2ind(frame2im(getframe(h)),256)                                      ;
        if time==time_start && index==idx_batch
            imwrite(imind,cm,NAME,'gif','Loopcount',inf     ,'DelayTime',PAUSE)
        else
            imwrite(imind,cm,NAME,'gif','WriteMode','append','DelayTime',PAUSE)
        end 
    end
    counter=counter+1                                                                      ;
end        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [idx_batch,idx_time]=get_ignore_indeces(PATH)
        value    =load(fullfile(PATH,strcat('ignore_period','.csv')))                      ;
        idx_batch=floor(value/100)+1                                                       ;
        idx_time =mod(value,100)                                                           ;
    end

    function INDECES=rFILENAMES(PATH)
        INDECES    =[]                                                                     ;
        LIST       =dir(PATH)                                                              ;  
        FILENAMES=string({LIST(4:end).name})                                               ;
        for name=1:size(FILENAMES,2)
            splits =split(FILENAMES{name},'_')                                             ;
            splits =split(splits{2}   ,'.')                                                ;
            INDECES=[INDECES,str2num(splits{1})]                                           ;
        end
        INDECES=sort(INDECES)                                                              ;
    end

    function MAXIMUM=get_MAXIMUM(PATH,INDECES)
        LIST=[]                                                                            ;
        for i=1:size(INDECES,2)
            FILENAME=fullfile(PATH,strcat('batch_',num2str(INDECES(i)),'.h5'))             ;
            DATA    =get_data(FILENAME)                                                    ;
            LIST    =[LIST,max(max(DATA))]                                                 ;
        end
        MAXIMUM     =max(LIST)                                                             ;
    end

    function EY=get_data(FILENAME)
        EY   =h5read (FILENAME,fullfile('/','ey'))                                         ;
        HZ   =h5read (FILENAME,fullfile('/','hz'))                                         ;
        EY   =permute(EY,[2,1]                   )                                         ;
        HZ   =permute(HZ,[2,1]                   )                                         ;
        POWER=0.5.*abs(HZ).*abs(EY)                                                        ;
    end

    function plotter(DATA,TIME,MAX)
        subplot('Position',[0 0 1 1])
        plot(DATA,'linewidth',3)
        text(0.1*size(DATA,1),0-0.7*MAX,num2str(TIME),...
             'FontSize'  ,10                         ,...
             'FontWeight','Bold'                         )
        axis([0 size(DATA,1) -MAX MAX])
        set(gca,'Visible','off')
    end      

end
