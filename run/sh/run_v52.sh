#! /bin/bash

cd run/FDTD1d

echo ' 1D TEz FREE SPCAE PROPAGATION'

rm     -rf                                   ../../simulations/v52
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v52

echo ' RUNNING SOLVER '
python solver.py -v v52 -pt 2 -p 2 -f 2000 -bw 50 -lw 50 -nt 2500

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v52',{0.05});exit;"


