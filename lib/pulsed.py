###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### PULSE GENERATION FUNCTION DEFINITIONS       ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import                numpy        as np
from   utilsd import (export_pulse        ,
                      export_ignore_period )

def get_pulse_gaussian(PARAMS,DT):
 freq =PARAMS[1][1]*1e6
 taw  =np.sqrt(2.3)/(np.pi*freq)
 t0   =np.sqrt(20)*taw
 steps=int(np.floor((10*taw)/DT))
 T    =np.array([i*DT for i in range(steps+1)])
 pulse=np.exp(-np.power((T-t0)/taw,2))
 return pulse, t0

def get_pulse_modulated_cosine_gaussian(PARAMS,DT):
 freq =PARAMS[1][1]*1e6
 BW   =PARAMS[1][2]/100
 df   =freq*BW
 taw  =np.sqrt(2.3)/(np.pi*(0.5*df))
 t0   =np.sqrt(20)*taw
 steps=int(np.floor((10*taw)/DT))
 T    =np.array([i*DT for i in range(steps+1)])
 pulse=np.cos((2*np.pi*freq)*(T-t0))*np.exp(-np.power((T-t0)/taw,2))
 return pulse, t0

def get_pulse(PARAMS,DT,PATHS):
 if  PARAMS[1][0] in [1]:
  pulse , delay =get_pulse_gaussian                 (PARAMS,DT)
 else:
  if PARAMS[1][0] in [2]:
   pulse, delay=get_pulse_modulated_cosine_gaussian(PARAMS,DT)
 export_pulse        (pulse,DT,PATHS,PARAMS            )
 export_ignore_period(int(np.floor(0.7*delay/DT)),PATHS)
 pulse_size=pulse.shape[0]
 print(' PULSE SAMPLES: '+str(pulse_size))
 if np.less_equal(pulse_size,PARAMS[3][0]):
  PULSE              =np.zeros((PARAMS[3][0]))
  PULSE[0:pulse_size]=pulse
 else:
   PULSE=pulse[0:PARAMS[3][0]]
 return PULSE


