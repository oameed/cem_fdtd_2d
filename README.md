# Finite Difference Time Domain (FDTD) for 2D TEz Problems

* This project demonstrates application of the FDTD method to cavity and scattering problems:
  * 2D cavities  : rectangular, ridged rectangular and circular 
  * 2D scattering: PEC & Dielectric circular rectangular and wedge cylinders

* Loop-Parallelism is applied to a simplifid version of the solver (i.e. only point source excitation) using Python's `multiprocessing` package.



## References

_Multiprocessing_

[[1].](https://research.wmz.ninja/articles/2018/03/on-sharing-large-arrays-when-using-pythons-multiprocessing.html) 2018. Wang. On Sharing Large Arrays When Using Python's Multiprocessing

_FDTD_

[2]. 2011. Inan. _Numerical Electromagnetics: The FDTD Method_  
[3]. 2009. Elsherbeni. Demir. _The FDTD Method for Electromagnetics_ 2nd Edition  
[4]. 2007. Berenger. _Perfectly Matched Layer (PML) for Computational Electromagnetics_  
[5]. 2005. Taflove. Hagness. _Computational Electrodynamics_ 3rd Edition  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.82  T=0.03 s (1037.0 files/s, 66164.8 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          10            141            147           1033
MATLAB                           2             25             23            195
Bourne Shell                    18            126              0            180
Markdown                         1             31              0             77
-------------------------------------------------------------------------------
SUM:                            31            323            170           1485
-------------------------------------------------------------------------------
</pre>

## How to Run

* To run simulations:  
  Using simulation `v11` as an example:
  1. `cd` to the main project directory
  2. `./run/sh/run_v11.sh`
  
* For `v1x` and `v2x` simulations, both serial and parallel solvers can be used by switching between `solver.py` and `solver_PARL.py` in the shell scripts.

## v1x: Rectangular/Ridged/Circular Cavities

|     |     |     |
|:---:|:---:|:---:|
Lx = 1 (m), Ly = 1 (m), [\[Pulse\]](simulations/v11/png/pulse.png) | Lx = 1 (m), Ly = 0.5 (m), [\[Pulse\]](simulations/v12/png/pulse.png) | R = 0.38 (m) [\[Pulse\]](simulations/v13/png/pulse.png) |  
![][fig_v11]                        | ![][fig_v12]                          | ![][fig_v13]          |

[fig_v11]:simulations/v11/gif/data.gif
[fig_v12]:simulations/v12/gif/data.gif
[fig_v13]:simulations/v13/gif/data.gif


## v2x: Point Source Excitation: Scattering from PEC/Dielectric Circular/Rectangular/Wedge Cylinders  

|     |     |     |
|:---:|:---:|:---:|
R = 0.5 (LAMBDAmin) [\[Pulse\]](simulations/v21/png/pulse.png)            | A (2R) = 1 (LAMBDAmin) [\[Pulse\]](simulations/v22/png/pulse.png)           | R = 0.75 (LAMBDAmin)            [\[Pulse\]](simulations/v23/png/pulse.png) |
![][fig_v21]                               | ![][fig_v22]                                 | ![][fig_v23]                     |
R = 0.5 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v24/png/pulse.png) | A(2R) = 1 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v25/png/pulse.png) | R = 0.75 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v26/png/pulse.png) |
![][fig_v24]                               | ![][fig_v25]                                 | ![][fig_v26]                     |

[fig_v21]:simulations/v21/gif/data.gif
[fig_v22]:simulations/v22/gif/data.gif
[fig_v23]:simulations/v23/gif/data.gif
[fig_v24]:simulations/v24/gif/data.gif
[fig_v25]:simulations/v25/gif/data.gif
[fig_v26]:simulations/v26/gif/data.gif


## v3x: TF/SF Excitation: Scattering from PEC/Dielectric Circular/Rectangular/Wedge Cylinders

* TF/SF Excitation in Free Space: [\[view simulation\]](simulations/v30/gif/data.gif)

|     |     |     |
|:---:|:---:|:---:|
R = 0.5 (LAMBDAmin) [\[Pulse\]](simulations/v31/png/pulse.png)            | A (2R) = 1 (LAMBDAmin) [\[Pulse\]](simulations/v32/png/pulse.png)           | R = 0.75 (LAMBDAmin)            [\[Pulse\]](simulations/v33/png/pulse.png) |
![][fig_v31]                               | ![][fig_v32]                                 | ![][fig_v33]                     |
R = 0.5 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v34/png/pulse.png) | A(2R) = 1 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v35/png/pulse.png) | R = 0.75 (LAMBDAmin), EPSr = 10 [\[Pulse\]](simulations/v36/png/pulse.png) |
![][fig_v34]                               | ![][fig_v35]                                 | ![][fig_v36]                     |


[fig_v31]:simulations/v31/gif/data.gif
[fig_v32]:simulations/v32/gif/data.gif
[fig_v33]:simulations/v33/gif/data.gif
[fig_v34]:simulations/v34/gif/data.gif
[fig_v35]:simulations/v35/gif/data.gif
[fig_v36]:simulations/v36/gif/data.gif


## Simulations v5x: 1D TEz Cavity/Free Space Propagation

|     |
|:--- |
[\[Pulse\]](simulations/v51/png/pulse.png), Lx = 1 (m) |
![][fig_v51]            |
[\[Pulse\]](simulations/v52/png/pulse.png)             |
![][fig_v52]            | 

[fig_v51]:simulations/v51/gif/data.gif
[fig_v52]:simulations/v52/gif/data.gif

