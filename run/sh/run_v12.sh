#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz RECTANGULAR RIDGED CAVITY '

rm     -rf                                   ../../simulations/v12
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v12

echo ' RUNNING SOLVER '
python solver_PARL.py -v v12 -pt 1 -p 1 -f 2000 -lx 1 -ly 0.5 -nt 1000 -obj 6

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v12',{0.05,false,'P',false});exit;"


