#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz CIRCULAR CAVITY '

rm     -rf                                   ../../simulations/v13
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v13

echo ' RUNNING SOLVER '
python solver_PARL.py -v v13 -pt 1 -p 1 -f 2000 -lx 1 -ly 1 -nt 1000 -obj 1

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v13',{0.05,false,'P',false});exit;"


