###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### OBJECT FUNCTION DEFINITIONS                 ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import             numpy          as np
from utilsd import get_lambda_min

def get_object_rect_to_circ_corners(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 conductivity =1e8
 rows         =MPEX.shape[0]
 columns      =MPEX.shape[1]
 center_row   =int(      rows   /2 )
 center_column=int(      columns/2 )
 radius       =int(0.75*(rows   /2))
 for row     in range(rows   ):
  for column in range(columns):
   lef_hand_side=np.power(row-center_row,2)+np.power(column-center_column,2)
   if np.greater_equal(lef_hand_side,np.power(radius,2)):
    MPEX[row,column,1]=conductivity
    MPEY[row,column,1]=conductivity
 #ViewB,ViewE =GRIDSPEC[6]
 #plt.clf()
 #plt.imshow(MPEX[ViewB:ViewE,ViewB:ViewE,1])
 #plt.show()
 return MPEX, MPEY, MPHZ

def get_object_cylinder_circ_pec(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 conductivity                =1e8
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.5)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 for  row    in range(rows   ):
  for column in range(columns):
   lef_hand_side=np.power(row-obj_center_row,2)+np.power(column-obj_center_col,2)
   if np.less_equal(lef_hand_side,np.power(radius,2)):
    MPEX[row,column,1]=conductivity
    MPEY[row,column,1]=conductivity
 #ex_col               =int(center_column-(1)*obj_center_displacement_col)
 #ex_row               =int(center_row   +(1)*obj_center_displacement_row)
 #ex_col_ratio         =ex_col/columns
 #ex_row_ratio         =ex_row/rows
 #MPEX[ex_row,ex_col,1]=1
 #MPEX[GRIDSPEC[4][0][0],GRIDSPEC[4][0][1],1]=1
 #ViewB,ViewE =GRIDSPEC[6]
 #plt.clf()
 #plt.imshow(MPEX[ViewB:ViewE,ViewB:ViewE,1])
 #plt.show()
 return MPEX, MPEY, MPHZ

def get_object_cylinder_circ_dielectric(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 dielectric                  =10
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.5)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 for  row    in range(rows   ):
  for column in range(columns):
   lef_hand_side=np.power(row-obj_center_row,2)+np.power(column-obj_center_col,2)
   if np.less_equal(lef_hand_side,np.power(radius,2)):
    MPEX[row,column,0]=MPEX[row,column,0]*dielectric
    MPEY[row,column,0]=MPEY[row,column,0]*dielectric
 return MPEX, MPEY, MPHZ

def get_object_cylinder_rect_pec(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 conductivity                =1e8
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.5)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 for  row    in range(rows   ):
  for column in range(columns):
   condition_x=np.less_equal(np.absolute(column-obj_center_col),radius)
   condition_y=np.less_equal(np.absolute(row   -obj_center_row),radius)
   if np.logical_and(condition_x,condition_y):
    MPEX[row,column,1]=conductivity
    MPEY[row,column,1]=conductivity
 return MPEX, MPEY, MPHZ

def get_object_cylinder_rect_dielectric(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 dielectric                  =10
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.5)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 for  row    in range(rows   ):
  for column in range(columns):
   condition_x=np.less_equal(np.absolute(column-obj_center_col),radius)
   condition_y=np.less_equal(np.absolute(row   -obj_center_row),radius)
   if np.logical_and(condition_x,condition_y):
    MPEX[row,column,0]=MPEX[row,column,0]*dielectric
    MPEY[row,column,0]=MPEY[row,column,0]*dielectric
 return MPEX, MPEY, MPHZ

def get_object_cylinder_wedge_pec(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 conductivity                =1e8
 angle_start                 =45*(1-0.5)
 angle_end                   =45*(1+0.5)
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.75)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 angle_start                 =angle_start*np.pi/180
 angle_end                   =angle_end  *np.pi/180
 for  row    in range(rows   ):
  for column in range(columns):
   lef_hand_side   =np.power(row-obj_center_row,2)+np.power(column-obj_center_col,2)
   if np.less_equal(lef_hand_side,np.power(radius,2)):
    if np.logical_and(row==obj_center_row,column==obj_center_col):
     MPEX[row,column,1]=conductivity
     MPEY[row,column,1]=conductivity
    else:
     if PARAMS[1][3]:
      condition_x     =np.greater(column,obj_center_col) 
      condition_y     =np.greater(row   ,obj_center_row)
      if np.logical_and(condition_x,condition_y):
       ARCTAN         =np.arctan((row-obj_center_row)/(column-obj_center_col))
       condition_start=np.greater_equal(ARCTAN,angle_start) 
       condition_end  =np.less_equal   (ARCTAN,angle_end  )
       if np.logical_and (condition_start,condition_end):
        MPEX[row,column,1]=conductivity
        MPEY[row,column,1]=conductivity
     else:
      condition_x=np.greater(column,obj_center_col) 
      condition_y=np.less   (row   ,obj_center_row) 
      if np.logical_and(condition_x,condition_y):
       ARCTAN         =np.arctan((row-obj_center_row)/(column-obj_center_col))
       condition_start=np.less_equal   (ARCTAN,-1*angle_start) 
       condition_end  =np.greater_equal(ARCTAN,-1*angle_end  )
       if np.logical_and (condition_start,condition_end):
        MPEX[row,column,1]=conductivity
        MPEY[row,column,1]=conductivity
 return MPEX, MPEY, MPHZ

def get_object_cylinder_wedge_dielectric(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 dielectric                  =10
 angle_start                 =45*(1-0.5)
 angle_end                   =45*(1+0.5)
 rows                        =MPEX.shape[0]
 columns                     =MPEX.shape[1]
 center_row                  =int(rows   /2)
 center_column               =int(columns/2)
 LAMBDA                      =get_lambda_min(PARAMS)/GRIDSPEC[0]
 radius                      =(0.75)*LAMBDA
 if not PARAMS[1][3]:
  obj_center_displacement_col=int((1.5*LAMBDA)*np.cos(np.pi/4))
  obj_center_displacement_row=int((1.5*LAMBDA)*np.cos(np.pi/4))
 else:
  obj_center_displacement_col=0
  obj_center_displacement_row=0
 obj_center_col              =center_column+obj_center_displacement_col
 obj_center_row              =center_row   -obj_center_displacement_row
 angle_start                 =angle_start*np.pi/180
 angle_end                   =angle_end  *np.pi/180
 for  row    in range(rows   ):
  for column in range(columns):
   lef_hand_side   =np.power(row-obj_center_row,2)+np.power(column-obj_center_col,2)
   if np.less_equal(lef_hand_side,np.power(radius,2)):
    if np.logical_and(row==obj_center_row,column==obj_center_col):
     MPEX[row,column,0]=MPEX[row,column,0]*dielectric
     MPEY[row,column,0]=MPEY[row,column,0]*dielectric
    else:
     if PARAMS[1][3]:
      condition_x     =np.greater(column,obj_center_col) 
      condition_y     =np.greater(row   ,obj_center_row)
      if np.logical_and(condition_x,condition_y):
       ARCTAN         =np.arctan((row-obj_center_row)/(column-obj_center_col))
       condition_start=np.greater_equal(ARCTAN,angle_start) 
       condition_end  =np.less_equal   (ARCTAN,angle_end  )
       if np.logical_and (condition_start,condition_end):
        MPEX[row,column,0]=MPEX[row,column,0]*dielectric
        MPEY[row,column,0]=MPEY[row,column,0]*dielectric
     else:
      condition_x=np.greater(column,obj_center_col) 
      condition_y=np.less   (row   ,obj_center_row) 
      if np.logical_and(condition_x,condition_y):
       ARCTAN         =np.arctan((row-obj_center_row)/(column-obj_center_col))
       condition_start=np.less_equal   (ARCTAN,-1*angle_start) 
       condition_end  =np.greater_equal(ARCTAN,-1*angle_end  )
       if np.logical_and (condition_start,condition_end):
        MPEX[row,column,0]=MPEX[row,column,0]*dielectric
        MPEY[row,column,0]=MPEY[row,column,0]*dielectric
 return MPEX, MPEY, MPHZ

def get_object_rect_to_ridged(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 conductivity        =1e8
 ridge_lower_x_begin =int(GRIDSPEC[2]*(0.5-0.25))
 ridge_lower_x_end   =int(GRIDSPEC[2]*(0.5+0.25))
 ridge_lower_y_begin =0
 ridge_lower_y_end   =int(GRIDSPEC[3]*(0.5-0.25))
 ridge_higher_x_begin=ridge_lower_x_begin
 ridge_higher_x_end  =ridge_lower_x_end
 ridge_higher_y_begin=int(GRIDSPEC[3]*(0.5+0.25))
 ridge_higher_y_end  =    GRIDSPEC[3]
 for  row    in range(ridge_lower_y_begin, ridge_lower_y_end):
  for column in range(ridge_lower_x_begin, ridge_lower_x_end):
   MPEX[row,column,1]=conductivity
   MPEY[row,column,1]=conductivity   
 for  row    in range(ridge_higher_y_begin, ridge_higher_y_end):
  for column in range(ridge_higher_x_begin, ridge_higher_x_end):
   MPEX[row,column,1]=conductivity
   MPEY[row,column,1]=conductivity   
 return MPEX, MPEY, MPHZ


