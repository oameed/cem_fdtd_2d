###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### SOLVER WITH SPLIT-PML ABC                   ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import numpy       as  np
from paramd import     PATHS, PARAMS
from Gridd  import    (get_grid          ,
                       get_indeces        )
from pulsed import     get_pulse
from CMatd  import     get_coefficients
from auxd   import    (get_AUX           ,
                       interpolate        )
from utilsd import    (wHDF              ,
                       get_milestones    ,
                       get_temp_container,
                       get_temp_index     )

grid_spec    =get_grid          (PARAMS                       )    
pulse        =get_pulse         (PARAMS,grid_spec[1],PATHS    )
Ex           =np.zeros          ((grid_spec[3],grid_spec[2]  ))
Ey           =np.zeros          ((grid_spec[3],grid_spec[2]  ))
Hz           =np.zeros          ((grid_spec[3],grid_spec[2],3))      # Hz[:,:,0]: Hz / Hz[:,:,1]: Hzx / Hz[:,:,2]: Hzy
Cex, Cey, Chz=get_coefficients  (PARAMS,grid_spec             )      # OBJECTS & PML ARE ADDED HERE
indeces      =get_indeces       (PARAMS,grid_spec             )

AUX          =get_AUX           (PARAMS,grid_spec             )      # TF/SF        EXCITATION: AUXILARY 1D WAVE & BOUNDARY INDECES

ex,ey,hz     =get_temp_container(grid_spec,PARAMS[7][0]       )      # TEMPORARY CONTAINERS FOR EXPORT
ViewB,ViewE  =grid_spec[6]                                           # VIEW WINDOW          FOR EXPORT
milestones   =get_milestones    (PARAMS[3][0]                 )      # KEEPING TRACK OF PROGRESS

print(' TIME STEPS   : '+str(PARAMS[3][0])                        ) 
print(' PROBLEM SIZE : '+str(grid_spec[2])+' x '+str(grid_spec[3]))

for NT in range(PARAMS[3][0]):
 
 
 for i in     AUX[6]:                                                # TF/SF        EXCITATION: AUXILIARY 1D WAVE: APPLY MAGNETIC SOURCE DISCONTINUITY
  AUX[1][i]  =AUX[1][i  ]+AUX[3][i,2]*pulse[NT]
 for i in     AUX[4]:                                                # TF/SF        EXCITATION: AUXILIARY 1D WAVE: UPDATE Hz
  AUX[1][i]  =AUX[3][i,0]*AUX[1][i  ]+AUX[3][i,1]*(AUX[0][i+1]-AUX[0][i  ])
 for i in     AUX[5]:                                                # TF/SF        EXCITATION: AUXILIARY 1D WAVE: UPDATE Ey
  AUX[0][i]  =AUX[2][i,0]*AUX[0][i  ]+AUX[2][i,1]*(AUX[1][i  ]-AUX[1][i-1])
 
 for count,index in enumerate(AUX[7][0]):                            # TF/SF        EXCITATION: APPLY REGION O-1 CONSISTENCY 
  j,i        =index
  Hz[j,i,0]  =Hz[j,i,0]-Chz[j,i,1]*interpolate(AUX[0][AUX[8][0][count][0]  ],
                                               AUX[0][AUX[8][0][count][0]+1],
                                               AUX[8][0       ][count][1]    )*(-np.sin(PARAMS[5][3]*np.pi/180))
 for count,index in enumerate(AUX[7][1]):                            # TF/SF        EXCITATION: APPLY REGION O-2 CONSISTENCY 
  j,i        =index
  Ey[j,i  ]  =Ey[j,i  ]+Cey[j,i,1]*interpolate(AUX[1][AUX[8][1][count][0]  ],
                                               AUX[1][AUX[8][1][count][0]+1],
                                               AUX[8][1       ][count][1]    )
  Hz[j,i,0]  =Hz[j,i,0]+Chz[j,i,2]*interpolate(AUX[0][AUX[8][2][count][0]  ],
                                               AUX[0][AUX[8][2][count][0]+1],
                                               AUX[8][2       ][count][1]    )*( np.cos(PARAMS[5][3]*np.pi/180))
 for count,index in enumerate(AUX[7][2]):                            # TF/SF        EXCITATION: APPLY REGION O-3 CONSISTENCY 
  j,i        =index
  Ex[j,i  ]  =Ex[j,i  ]+Cex[j,i,1]*interpolate(AUX[1][AUX[8][3][count][0]  ],
                                               AUX[1][AUX[8][3][count][0]+1],
                                               AUX[8][3       ][count][1]    )
  Hz[j,i,0]  =Hz[j,i,0]+Chz[j,i,1]*interpolate(AUX[0][AUX[8][4][count][0]  ],
                                               AUX[0][AUX[8][4][count][0]+1],
                                               AUX[8][4       ][count][1]    )*(-np.sin(PARAMS[5][3]*np.pi/180))
 for count,index in enumerate(AUX[7][3]):                            # TF/SF        EXCITATION: APPLY REGION O-4 CONSISTENCY 
  j,i        =index
  Hz[j,i,0]  =Hz[j,i,0]-Chz[j,i,2]*interpolate(AUX[0][AUX[8][5][count][0]  ],
                                               AUX[0][AUX[8][5][count][0]+1],
                                               AUX[8][5       ][count][1]    )*( np.cos(PARAMS[5][3]*np.pi/180))
 for count,index in enumerate(AUX[7][4]):                            # TF/SF        EXCITATION: APPLY REGION I-1 CONSISTENCY 
  j,i        =index
  Ex[j,i  ]  =Ex[j,i  ]-Cex[j,i,1]*interpolate(AUX[1][AUX[8][6][count][0]  ],
                                               AUX[1][AUX[8][6][count][0]+1],
                                               AUX[8][6       ][count][1]    ) 
 for count,index in enumerate(AUX[7][5]):                            # TF/SF        EXCITATION: APPLY REGION I-4 CONSISTENCY 
  j,i        =index
  Ey[j,i  ]  =Ey[j,i  ]-Cey[j,i,1]*interpolate(AUX[1][AUX[8][7][count][0]  ],
                                               AUX[1][AUX[8][7][count][0]+1],
                                               AUX[8][7       ][count][1]    )
 
 
 for index in grid_spec[4]   :                                       # # POINT SOURCE EXCITATION: APPLY MAGNETIC SOURCE DISCONTINUITY
  j,i        =index
  Hz[j,i,0]  =Hz[j,i,0]+Chz[j,i,3]*pulse[NT]
 
 
 for index in indeces  [0][0]:                                       # UPDATE Hz IN NON-PML REGION
  j,i        =index
  Hz[j,i,0]  =Chz[j,i,0]*Hz[j,i,0]+Chz[j,i,1]*(Ex[j+1,i]-Ex[j,i])+Chz[j,i,2]*(Ey[j,i+1]-Ey[j,i])
 for index in indeces  [0][1]:                                       # UPDATE Hz IN     PML REGION
  j,i        =index
  Hz[j,i,1]  =Chz[j,i,4]*Hz[j,i,1]+Chz[j,i,5]*(Ey[j,i+1]-Ey[j,i]) 
  Hz[j,i,2]  =Chz[j,i,6]*Hz[j,i,2]+Chz[j,i,7]*(Ex[j+1,i]-Ex[j,i])
  Hz[j,i,0]  =Hz [j,i,1]+Hz[j,i,2]
 
 for index in indeces  [1]   :                                       # UPDATE Ex
  j,i        =index
  Ex[j,i]    =Cex[j,i,0]*Ex[j,i]+Cex[j,i,1]*(Hz[j,i,0]-Hz[j-1,i  ,0])
 
 for index in indeces  [2]   :                                       # UPDATE Ey
  j,i        =index
  Ey[j,i]    =Cey[j,i,0]*Ey[j,i]+Cey[j,i,1]*(Hz[j,i,0]-Hz[j  ,i-1,0])
 
 
 temp_index        =get_temp_index(NT+1,PARAMS[7][0]) 
 ex[:,:,temp_index]=Ex[ViewB:ViewE,ViewB:ViewE  ]
 ey[:,:,temp_index]=Ey[ViewB:ViewE,ViewB:ViewE  ]
 hz[:,:,temp_index]=Hz[ViewB:ViewE,ViewB:ViewE,0]
 
 if (temp_index+1)==PARAMS[7][0]:
  wHDF(os.path.join(PATHS[1],'batch_'+str(NT)+'.h5'),
       ['ex', 'ey', 'hz'                           ],
       [ ex ,  ey ,  hz                            ] ) 
 
 if NT     in milestones[0]  :
  print(' PROGRESS ... '+str(milestones[1]*10)+' %')
  milestones[1]+=1

print(' FINISHED ')


