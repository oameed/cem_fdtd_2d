###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### COEFFICIENT MATRICES FUNCTION DEFINITIONS   ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import            numpy                            as np
from Objd import (get_object_rect_to_circ_corners     ,
                  get_object_cylinder_circ_pec        ,
                  get_object_cylinder_rect_pec        ,
                  get_object_cylinder_circ_dielectric ,
                  get_object_cylinder_rect_dielectric ,
                  get_object_cylinder_wedge_pec       ,
                  get_object_cylinder_wedge_dielectric,
                  get_object_rect_to_ridged            )

def get_objects(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ):
 if         PARAMS[4][0] in [0]:
  MPex=MPEX
  MPey=MPEY
  MPhz=MPHZ
 else:
  if        PARAMS[4][0] in [1]:
   MPex,MPey,MPhz =get_object_rect_to_circ_corners           (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
  else:
   if       PARAMS[4][0] in [2]:
    MPex,MPey,MPhz=get_object_cylinder_circ_pec              (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
   else:
    if      PARAMS[4][0] in [3]:
     MPex,MPey,MPhz=get_object_cylinder_rect_pec             (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
    else:
     if     PARAMS[4][0] in [4]:
      MPex,MPey,MPhz=get_object_cylinder_circ_dielectric     (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
     else:
      if    PARAMS[4][0] in [5]:
       MPex,MPey,MPhz=get_object_cylinder_rect_dielectric    (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
      else:
       if   PARAMS[4][0] in [6]:
        MPex,MPey,MPhz=get_object_rect_to_ridged             (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
       else:
        if  PARAMS[4][0] in [7]:
         MPex,MPey,MPhz=get_object_cylinder_wedge_pec        (PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
        else:
         if PARAMS[4][0] in [8]:
          MPex,MPey,MPhz=get_object_cylinder_wedge_dielectric(PARAMS,GRIDSPEC,MPEX,MPEY,MPHZ)
 return MPex, MPey, MPhz

def get_material(PARAMS,GRIDSPEC):
 def pml_fill_left  (B,E,SIGMAX,P,MP):
  for column in range(B,E+1):
   MP[:,column,1]=SIGMAX*np.power(((E-column)/(E-B)),P)
  return MP
 def pml_fill_right (B,E,SIGMAX,P,MP):
  for column in range(B,E+1):
   MP[:,column,1]=SIGMAX*np.power(((column-B)/(E-B)),P)
  return MP
 def pml_fill_bottom(B,E,SIGMAX,P,MP):
  for row in range(B,E+1):
   MP[row,:,1]   =SIGMAX*np.power(((E-row   )/(E-B)),P)
  return MP
 def pml_fill_top   (B,E,SIGMAX,P,MP):
  for row in range(B,E+1):
   MP[row,:,1]   =SIGMAX*np.power(((row-B   )/(E-B)),P)
  return MP
 MPex          =np.zeros((GRIDSPEC[3],GRIDSPEC[2],2))
 MPey          =np.zeros((GRIDSPEC[3],GRIDSPEC[2],2))
 MPhz          =np.zeros((GRIDSPEC[3],GRIDSPEC[2],4))
 MPex[:,:,0]   =PARAMS[-1][0]
 MPey[:,:,0]   =PARAMS[-1][0]
 MPhz[:,:,0]   =PARAMS[-1][1]
 MPex,MPey,MPhz=get_objects(PARAMS,GRIDSPEC,MPex,MPey,MPhz)
 if PARAMS[0][0] in [2]:
  R0           =1e-8
  P            =2
  sigma_max    =-(1/PARAMS[-1][2])*((P+1)*np.log(R0))/(2*GRIDSPEC[0]*GRIDSPEC[5][0][1])
  MPey         =pml_fill_left  (0                ,GRIDSPEC[5][0][1],sigma_max,P,MPey)
  MPey         =pml_fill_right (GRIDSPEC[5][1][0],GRIDSPEC[5][1][1],sigma_max,P,MPey)
  MPex         =pml_fill_bottom(0                ,GRIDSPEC[5][0][1],sigma_max,P,MPex)
  MPex         =pml_fill_top   (GRIDSPEC[5][1][0],GRIDSPEC[5][1][1],sigma_max,P,MPex)
  MPhz[:,:,2]  =(PARAMS[-1][1]/PARAMS[-1][0])*MPey[:,:,1]
  MPhz[:,:,3]  =(PARAMS[-1][1]/PARAMS[-1][0])*MPex[:,:,1]
 return MPex, MPey, MPhz

def get_coefficients(PARAMS,GRIDSPEC):
 MPex, MPey, MPhz=get_material(PARAMS,GRIDSPEC)
 Cex             =np.zeros((GRIDSPEC[3],GRIDSPEC[2],3))
 Cey             =np.zeros((GRIDSPEC[3],GRIDSPEC[2],3))
 Chz             =np.zeros((GRIDSPEC[3],GRIDSPEC[2],8))
 Cex[:,:,0]      = (2*MPex[:,:,0]-GRIDSPEC[1]*MPex[:,:,1])/( 2*MPex[:,:,0]+GRIDSPEC[1]*MPex[:,:,1]             )
 Cex[:,:,1]      = (2*            GRIDSPEC[1]            )/((2*MPex[:,:,0]+GRIDSPEC[1]*MPex[:,:,1])*GRIDSPEC[0])
 Cex[:,:,2]      =-(2*            GRIDSPEC[1]            )/( 2*MPex[:,:,0]+GRIDSPEC[1]*MPex[:,:,1]             )
 Cey[:,:,0]      = (2*MPey[:,:,0]-GRIDSPEC[1]*MPey[:,:,1])/( 2*MPey[:,:,0]+GRIDSPEC[1]*MPey[:,:,1]             ) 
 Cey[:,:,1]      =-(2*            GRIDSPEC[1]            )/((2*MPey[:,:,0]+GRIDSPEC[1]*MPey[:,:,1])*GRIDSPEC[0])
 Cey[:,:,2]      =-(2*            GRIDSPEC[1]            )/( 2*MPey[:,:,0]+GRIDSPEC[1]*MPey[:,:,1]             )
 Chz[:,:,0]      = (2*MPhz[:,:,0]-GRIDSPEC[1]*MPhz[:,:,1])/( 2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,1]             )
 Chz[:,:,1]      = (2*            GRIDSPEC[1]            )/((2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,1])*GRIDSPEC[0])
 Chz[:,:,2]      =-(2*            GRIDSPEC[1]            )/((2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,1])*GRIDSPEC[0])
 Chz[:,:,3]      =-(2*            GRIDSPEC[1]            )/ (2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,1]             )
 Chz[:,:,4]      = (2*MPhz[:,:,0]-GRIDSPEC[1]*MPhz[:,:,2])/( 2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,2]             )
 Chz[:,:,5]      =-(2*            GRIDSPEC[1]            )/((2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,2])*GRIDSPEC[0])
 Chz[:,:,6]      = (2*MPhz[:,:,0]-GRIDSPEC[1]*MPhz[:,:,3])/( 2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,3]             )
 Chz[:,:,7]      = (2*            GRIDSPEC[1]            )/((2*MPhz[:,:,0]+GRIDSPEC[1]*MPhz[:,:,3])*GRIDSPEC[0])
 return Cex, Cey, Chz

def get_coefficients_1d(PARAMS,GRIDSPEC):
 def get_materials_1d  (PARAMS,GRIDSPEC):
  def pml_fill_left_1d (B,E,SIGMAX,P,MP):
   for column in range(B,E+1):
    MP[column,1]=SIGMAX*np.power(((E-column)/(E-B)),P)
   return MP
  def pml_fill_right_1d(B,E,SIGMAX,P,MP):
   for column in range(B,E+1):
    MP[column,1]=SIGMAX*np.power(((column-B)/(E-B)),P)
   return MP
  MPey        =np.zeros((GRIDSPEC[2],2))
  MPhz        =np.zeros((GRIDSPEC[2],2))
  MPey[:,0]   =PARAMS[-1][0]
  MPhz[:,0]   =PARAMS[-1][1]
  if PARAMS[0][0] in [2]:
   R0         =1e-8
   P          =2
   sigma_max  =-(1/PARAMS[-1][2])*((P+1)*np.log(R0))/(2*GRIDSPEC[0]*GRIDSPEC[5][0][1])
   MPey       =pml_fill_left_1d (0                ,GRIDSPEC[5][0][1],sigma_max,P,MPey)
   MPey       =pml_fill_right_1d(GRIDSPEC[5][1][0],GRIDSPEC[5][1][1],sigma_max,P,MPey)
   MPhz[:,1]  =(PARAMS[-1][1]/PARAMS[-1][0])*MPey[:,1]
  return MPey, MPhz
 MPey,MPhz=get_materials_1d(PARAMS,GRIDSPEC)
 Cey      =np.zeros((GRIDSPEC[2],3))
 Chz      =np.zeros((GRIDSPEC[2],3))
 Cey[:,0] = (2*MPey[:,0]-GRIDSPEC[1]*MPey[:,1])/( 2*MPey[:,0]+GRIDSPEC[1]*MPey[:,1]             ) 
 Cey[:,1] =-(2*          GRIDSPEC[1]          )/((2*MPey[:,0]+GRIDSPEC[1]*MPey[:,1])*GRIDSPEC[0])
 Cey[:,2] =-(2*          GRIDSPEC[1]          )/( 2*MPey[:,0]+GRIDSPEC[1]*MPey[:,1]             )
 Chz[:,0] = (2*MPhz[:,0]-GRIDSPEC[1]*MPhz[:,1])/( 2*MPhz[:,0]+GRIDSPEC[1]*MPhz[:,1]             )
 Chz[:,1] =-(2*          GRIDSPEC[1]          )/((2*MPhz[:,0]+GRIDSPEC[1]*MPhz[:,1])*GRIDSPEC[0])
 Chz[:,2] =-(2*          GRIDSPEC[1]          )/ (2*MPhz[:,0]+GRIDSPEC[1]*MPhz[:,1]             )
 return Cey, Chz


# MPex[:,:,0]: PERMATIVITTY
# MPex[:,:,1]: ELECTRIC CONDUCTIVITY IN NON-PML REGION / IN PML REGION: sigma_e_y

# MPey[:,:,0]: PERMATIVITTY
# MPey[:,:,0]: ELECTRIC CONDUCTIVITY IN NON-PML REGION / IN PML REGION: sigma_e_x

# MPhz[:,:,0]: PERMEABILITY
# MPhz[:,:,1]: MAGNETIC CONDUCTIVITY IN NON-PML REGION
# MPhz[:,:,2]: MAGNETIC CONDUCTIVITY IN     PML REGION: sigma_m_x 
# MPhz[:,:,3]: MAGNETIC CONDUCTIVITY IN     PML REGION: sigma_m_y

# Cex[:,:,0] : Cexe
# Cex[:,:,1] : Cexh
# Cex[:,:,2] : Cexj

# Cey[:,:,0] : Ceye
# Cey[:,:,1] : Ceyh
# Cey[:,:,2] : Ceyj

# Chz[:,:,0]: Chzh   (NON-PML REGION)
# Chz[:,:,1]: Chzex  (NON-PML REGION)
# Chz[:,:,2]: Chzey  (NON-PML REGION)
# Chz[:,:,3]: Chzm   (NON-PML REGION)
# Chz[:,:,4]: Chzhzx (    PML REGION)
# Chz[:,:,5]: Chzey  (    PML REGION)
# Chz[:,:,6]: Chzhzy (    PML REGION)
# Chz[:,:,7]: Chzex  (    PML REGION)

# OBJECT TYPE (0): NO OBJECT 
# OBJECT TYPE (1): RECT-TO-CIRC
# OBJECT TYPE (2): PEC        CIRCULAR    CYLINDER
# OBJECT TYPE (3): PEC        RECTANGULAR CYLINDER
# OBJECT TYPE (4): DIELECTRIC CIRCULAR    CYLINDER
# OBJECT TYPE (5): DIELECTRIC RECTANGULAR CYLINDER
# OBJECT TYPE (6): RECT-TO-RIDGED
# OBJECT TYPE (7): PEC        WEDGE       CYLINDER
# OBJECT TYPE (8): DIELECTRIC WEDGE       CYLINDER

