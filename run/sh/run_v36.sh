#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM DIELECTRIC        WEDGE       CYLINDER '

rm     -rf                                   ../../simulations/v36
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v36

echo ' RUNNING SOLVER '
python solver.py -v v36 -pt 2 -p 1 -f 2000 -nt 1000 -ncpw 50 -obj 8 -tf

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v36',{0.05,true,'Emag',true});exit;"


