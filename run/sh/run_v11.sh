#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz RECTANGULAR CAVITY '

rm     -rf                                   ../../simulations/v11
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v11

echo ' RUNNING SOLVER '
python solver_PARL.py -v v11 -pt 1 -p 1 -f 2000 -lx 1 -ly 1 -nt 1000

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v11',{0.05,false,'P',false});exit;"


