#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM DIELECTRIC WEDGE       CYLINDER '

rm     -rf                                   ../../simulations/v26
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v26

echo ' RUNNING SOLVER '
python solver_PARL.py -v v26 -pt 2 -p 1 -f 2000 -nt 1000 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 8

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v26',{0.05,false,'P',false});exit;"


