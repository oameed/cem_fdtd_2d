#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM DIELECTRIC CIRCULAR    CYLINDER '

rm     -rf                                   ../../simulations/v24
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v24

echo ' RUNNING SOLVER '
python solver_PARL.py -v v24 -pt 2 -p 1 -f 2000 -nt 1000 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 4

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v24',{0.05,false,'P',false});exit;"


