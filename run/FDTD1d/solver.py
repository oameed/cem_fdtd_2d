###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 1D TEz ###
### SOLVER WITH SPLIT-PML ABC                   ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import numpy       as  np
from paramd import     PATHS, PARAMS
from Gridd  import     get_grid
from pulsed import     get_pulse
from CMatd  import     get_coefficients_1d
from utilsd import    (wHDF                 ,
                       get_temp_container_1d,
                       get_milestones       ,
                       get_temp_index        )

grid_spec  =get_grid           (PARAMS                   )    
pulse      =get_pulse          (PARAMS,grid_spec[1],PATHS)
Ey         =np.zeros           ((grid_spec[2])           )
Hz         =np.zeros           ((grid_spec[2])           )
Cey, Chz   =get_coefficients_1d(PARAMS,grid_spec         )  # PML IS ADDED HERE

ey,hz      =get_temp_container_1d(grid_spec,PARAMS[7][0])   # TEMPORARY CONTAINERS FOR EXPORT
ViewB,ViewE=grid_spec[6]                                    # VIEW WINDOW          FOR EXPORT

milestones =get_milestones    (PARAMS[3][0]              )  # KEEPING TRACK OF PROGRESS

print(' TIME STEPS   : '+str(PARAMS[3][0])               ) 
print(' PROBLEM SIZE : '+str(grid_spec[2])               )

for NT in range(PARAMS[3][0]    ):
 
 i     =grid_spec[4][0][1]                                  # APPLY MAGNETIC SOURCE DISCONTINUITY
 Hz[i] =Hz[i]+Chz[i,2]*pulse[NT]
 
 for i in range(  grid_spec[2]-1):                          # UPDATE Hz
  Hz[i]=Chz[i,0]*Hz[i]+Chz[i,1]*(Ey[i+1]-Ey[i])
 
 for i in range(1,grid_spec[2]-1):                          # UPDATE Ey
  Ey[i]=Cey[i,0]*Ey[i]+Cey[i,1]*(Hz[i]-Hz[i-1])

 temp_index      =get_temp_index(NT+1,PARAMS[7][0])
 ey[:,temp_index]=Ey[ViewB:ViewE]
 hz[:,temp_index]=Hz[ViewB:ViewE]
 
 if (temp_index+1)==PARAMS[7][0]:
  wHDF(os.path.join(PATHS[1],'batch_'+str(NT)+'.h5'),
       ['ey', 'hz'                                 ],
       [ ey ,  hz                                  ] ) 
 
 if NT     in milestones[0]  :
  print(' PROGRESS ... '+str(milestones[1]*10)+' %')
  milestones[1]+=1

print(' FINISHED ')


