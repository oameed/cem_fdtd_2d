###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### PARAMETER DEFINITIONS                       ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import argparse
import numpy    as np

### DEFINE PARSER
parser =argparse.ArgumentParser()

### DEFINE PARAMETERS
parser.add_argument('-v'    , type=str   ,               help='SIMULATION VERSION'                                    , required=True        )
parser.add_argument('-pt'   , type=int   ,               help='PROBLEM TYPE: RESONATOR (1) /SCATTERING (2)'           , required=True        )
parser.add_argument('-ncpw' , type=int   , default=40  , help='NUMBER OF CELL PER WAVELENGTH'                                                )
 # PULSE
parser.add_argument('-p'    , type=int   ,               help='PULSE TYPE: GAUSSIAN (1) / MODULATED COSINE (2)'       , required=True        )
parser.add_argument('-f'    , type=float ,               help='FREQUENCY IN MHz. GAUSSIAN: Fmax, MODULATED COSINE: Fc'                       )
parser.add_argument('-bw'   , type=float , default=20  , help='FRACTIONAL BANDWIDTH OF MODCOS PULSE IN PERCENT(%)'                           )
parser.add_argument('-tf'   ,                            help='TOTAL FIELD / SCATTER FIELD'                           , action  ='store_true')
parser.add_argument('-ang'  , type=float , default=45  , help='ANGLE OF INCIDENCE FOR TF/SF EXCITATION'                                      )
 # GEOMETRY & TIME
parser.add_argument('-lx'   , type=float , default=1   , help='LENGTH OF GEOMETRIC DOMAIN ALONG X IN m'                                      )
parser.add_argument('-ly'   , type=float , default=1   , help='LENGTH OF GEOMETRIC DOMAIN ALONG Y IN m'                                      )
parser.add_argument('-nt'   , type=int   ,               help='TOTAL NUMBER OF TIME-STEPS'                            , required=True        )
parser.add_argument('-lw'   , type=float , default=10  , help='LENGTH OF GEOMETRIC DOMAIN AS A MULTIPLE OF WAVELENGTH'                       )
 # OBJECTS
parser.add_argument('-obj'  , type=int   , default=0   , help='OBJECT TYPE'                                                                  )
 # DOMAIN DESIGN
parser.add_argument('-ppx'  , type=float , default=0.5 , help='PULSE POSITION FACTOR ALONG X AXIS'                                           )
parser.add_argument('-ppy'  , type=float , default=0.5 , help='PULSE POSITION FACTOR ALONG Y AXIS'                                           )

### ENABLE FLAGS
args   = parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

eps0   =8.854e-12
mu0    =4*np.pi*1e-7

PATHS  =[os.path.join('..','..','simulations',args.v       ),
         os.path.join('..','..','simulations',args.v,'data') ]

PARAMS =[[args.pt  , args.ncpw        , 0.5                ],
         [args.p   , args.f           , args.bw, args.tf   ],
         [args.lx  , args.ly          , args.lw            ],
         [args.nt                                          ],
         [args.obj                                         ],
         [0.25     , 0.40             , 0.60   , args.ang  ],
         [args.ppx , args.ppy                              ],
         [100                                              ],
         [eps0, mu0, np.sqrt(mu0/eps0), 1/np.sqrt(mu0*eps0)] ]


# PARAMS[0][0]: PROBLEM TYPE: RESONATOR (1) / SCATTERING (2)
# PARAMS[0][1]: NUMBER OF CELLS PER SMALLEST WAVELENGTH
# PARAMS[0][2]: CFL FACTOR
# PARAMS[1][0]: PULSE TYPE: GAUSSIAN (1) / MODULATED COSINE GAUSSIAN (2)
# PARAMS[1][1]: FREQUENCY (MHz) : MAX FREQUENCY FOR PULSE TYPE (1) / CENTER FREQUENCY FOR PULSE TYPE (2)
# PARAMS[1][2]: BANDWIDTH FOR PULSE TYPE (2)
# PARAMS[1][3]: APPLY EXCITATION AS TF/SF FOR PROBLEM TYPE (2)
# PARAMS[2][0]: LENGTH OF DOMAIN IN THE DIRECTION OF X AXIS (m)
# PARAMS[2][1]: LENGTH OF DOMAIN IN THE DIRECTION OF X AXIS (m)
# PARAMS[2][2]: LENGTH OF DOMAIN IN MULTIPLES OF WAVELENGTH FOR PROBLEM TYPE (2)
# PARAMS[3][0]: NUMBER OF TIME-STEPS TO RUN THE SIMULATION
# PARAMS[4][0]: OBJECT TYPE
# PARAMS[5][0]: FRACTION OF THE DOMAIN LENGTH TO APPLY PML          FOR PROBLEM TYPE (2)
# PARAMS[5][1]: FRACTIONAL POSITION OF THE BEGINNIG OF TF/SF REGION FOR PROBLEM TYPE (2)
# PARAMS[5][2]: FRACTIONAL POSITION OF THE END      OF TF/SF REGION FOR PROBLEM TYPE (2)
# PARAMS[5][3]: ANGLE OF INCIDENCE FOR TF/SF EXCITATION (Deg)
# PARAMS[6][0]: FRACTIONAL POSITION OF THE POINT SOURCE ALONG THE X AXIS
# PARAMS[6][1]: FRACTIONAL POSITION OF THE POINT SOURCE ALONG THE Y AXIS
# PARAMS[7][0]: TEMPPRARY CONTAINER SIZE
# PARAMS[8][1]: FREE SPACE PERMITIVITY
# PARAMS[8][2]: FREE SPACE PERMEABILITY
# PARAMS[8][3]: FREE SPACE IMPEDANCE
# PARAMS[8][4]: SPEED OF LIGHT IN FREE SPACE


