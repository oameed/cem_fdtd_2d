#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM PEC        RECTANGULAR CYLINDER '

rm     -rf                                   ../../simulations/v32
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v32

echo ' RUNNING SOLVER '
python solver.py -v v32 -pt 2 -p 1 -f 2000 -nt 700 -ncpw 50 -obj 3 -tf

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v32',{0.05,true,'Emag',true});exit;"


