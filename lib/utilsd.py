###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### UTILITIES FUNCTION  DEFINITIONS             ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import numpy as np
import h5py

def get_lambda_min(PARAMS):
 if  PARAMS[1][0] in [1]:
  freq       =PARAMS[1][1]*1e6
  lambda_min =PARAMS[-1][-1]/freq
 else:
  if PARAMS[1][0] in [2]:
   freq      =PARAMS[1][1]*1e6
   BW        =PARAMS[1][2]/100
   freq_max  =freq*(1+0.5*BW)
   lambda_min=PARAMS[-1][-1]/freq_max
 return lambda_min

def export_pulse(PULSE,DT,PATHS,PARAMS):
 import                 numpy  as np
 from matplotlib import pyplot as plt
 def get_FFT(PULSE,DT):
  resolution=1e5
  FFT       =np.fft.fft     (PULSE, n=int(resolution) )
  FFT       =np.fft.fftshift(FFT                      )
  FFT       =np.absolute(FFT)/np.amax(np.absolute(FFT))
  FreqAxis  =np.linspace(-0.5,0.5,FFT.shape[0])*(1/DT)
  return FFT, FreqAxis
 def get_pulse_PLOT(PULSE,T,FFT,FREQAXIS,CONFIG):
  freq=CONFIG[1]
  bw  =CONFIG[2]/100
  fig =plt.figure(figsize=(10*1.35,5*1.35)) 
  ax  =plt.subplot(2,1,1)
  ax.plot(T/1e-9,PULSE/np.amax(np.absolute(PULSE)),linewidth=3)
  ax.set_xlabel('Time (ns)')
  ax  =plt.subplot(2,1,2)
  ax.plot  (FreqAxis/1e6,FFT                      ,linewidth=3)
  if  CONFIG[0] in [1]:
   plt.xlim  ([0,int(freq*1.25)])
   plt.xticks([0,freq],[str(0),str(freq)])
  else:
   if CONFIG[0] in [2]:
    plt.xlim  ([0,1.25*(freq*(1+0.5*bw))])
    plt.xticks([0,freq*(1-0.5*bw),freq,freq*(1+0.5*bw)],[str(0),str(freq*(1-0.5*bw)),str(freq),str(freq*(1+0.5*bw))])
  plt.ylim  ([0,1                  ])
  plt.yticks([0,0.1      ,1],[str(0),str(0.1 )     ,str(1)])
  plt.grid  (b=True)
  ax.set_xlabel('Freq (MHz)')
  return fig
 T           =np.array([i*DT for i in range(PULSE.shape[0])])
 FFT,FreqAxis=get_FFT(PULSE,DT)
 fig         =get_pulse_PLOT(PULSE,T,FFT,FreqAxis,PARAMS[1][0:3])
 plt.savefig(os.path.join(PATHS[0],'png','pulse'+'.png'),format='png')

def export_ignore_period(VALUE,PATHS):
 FILENAME=os.path.join(PATHS[0],'ignore_period'+'.csv')
 with open(FILENAME, 'w') as fobj:
  fobj.write(str(VALUE))

def get_milestones(NT):
 milestones=[int(NT*0.1*i) for i in range(1,10)]
 counter   =1
 return [milestones,counter]

def wHDF(FILENAME,DIRNAMES,DATA):
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def get_temp_container(GRIDSPEC,SIZE):
 ViewB,ViewE=GRIDSPEC[6]
 temp_array =np.zeros ((GRIDSPEC[3],GRIDSPEC[2]))
 temp_array =temp_array[ViewB:ViewE,ViewB:ViewE]
 rows       =temp_array.shape[0]
 columns    =temp_array.shape[1]
 ex         =np.zeros((rows,columns,SIZE)) 
 ey         =np.zeros((rows,columns,SIZE))
 hz         =np.zeros((rows,columns,SIZE))
 return ex, ey, hz

def get_temp_index(COUNTER,SIZE):
 REMAINDER=np.mod(COUNTER,SIZE)
 if REMAINDER==0:
  temp_index=SIZE-1
 else:
  temp_index=REMAINDER-1
 return temp_index

def get_temp_container_1d(GRIDSPEC,SIZE):
 ViewB,ViewE=GRIDSPEC[6]
 temp_array =np.zeros ((GRIDSPEC[2]))
 temp_array =temp_array[ViewB:ViewE]
 columns    =temp_array.shape[0]
 ey         =np.zeros ((columns,SIZE))
 hz         =np.zeros ((columns,SIZE))
 return ey, hz

def buffer_to_np(X   ,SHAPE  ):
 return np.frombuffer(X, dtype=np.float64).reshape(SHAPE)

def write_buffer(X_NP,DATA   ):
 np.copyto(X_NP, DATA)


