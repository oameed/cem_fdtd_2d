#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz TF/SF EXCITATION IN FREE SPACE '

rm     -rf                                   ../../simulations/v30
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v30

echo ' RUNNING SOLVER '
python solver.py -v v30 -pt 2 -p 1 -f 2000 -nt 1000 -ncpw 50 -tf

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v30',{0.05,true,'P',true});exit;"


