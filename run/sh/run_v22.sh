#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM PEC        RECTANGULAR CYLINDER '

rm     -rf                                   ../../simulations/v22
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v22

echo ' RUNNING SOLVER '
python solver_PARL.py -v v22 -pt 2 -p 1 -f 2000 -nt 700 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 3

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v22',{0.05,false,'P',false});exit;"


