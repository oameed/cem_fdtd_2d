###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### GRID FUNCTION  DEFINITIONS                  ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import             numpy          as np
from utilsd import get_lambda_min

def get_grid(PARAMS):
 def get_lx_ly(PARAMS,LAMBDA):
  if  PARAMS [0][0] in [1]:
   lx =PARAMS[2][0]
   ly =PARAMS[2][1]
  else:
   if PARAMS [0][0] in [2]:
    lx=PARAMS[2][2]*LAMBDA
    ly=lx
  return lx,ly
 def get_pml_indeces(PARAMS,N):
  pml_thickness      =int(np.floor(N*PARAMS[5][0]))
  pml_left_begin_end =[0,pml_thickness-1]
  pml_right          =[(N-1)-i for i in range(pml_thickness)]
  pml_right.sort()
  pml_right_begin_end=[pml_right[0],pml_right[-1]]
  return [pml_left_begin_end,pml_right_begin_end]
 def get_tfsf_indeces(PARAMS,N):
  tfsf_begin=int(np.floor(N*PARAMS[5][1]))
  tfsf_end  =int(np.floor(N*PARAMS[5][2]))
  return [tfsf_begin,tfsf_end]
 lambda_min =get_lambda_min(PARAMS)
 dx         =lambda_min/PARAMS[0][1]                             # CELL      SIZE
 dt         =(dx/PARAMS[-1][-1])*PARAMS[0][2]                    # TIME-STEP SIZE
 lx,ly      =get_lx_ly(PARAMS,lambda_min)
 Nx         =int(np.floor( lx/dx              ))                 # GRID SIZE      IN DIRECTION OF X AXIS
 Ny         =int(np.floor( ly/dx              ))                 # GRID SIZE      IN DIRECTION OF Y AXIS
 grid_spec  =[dx, dt, Nx, Ny]
 if  PARAMS[0][0] in [1]:
  ppx       =int(np.floor((lx*PARAMS[6][0])/dx))                 # PULSE POSITION IN DIRECTION OF X AXIS
  ppy       =int(np.floor((ly*PARAMS[6][1])/dx))                 # PULSE POSITION IN DIRECTION OF Y AXIS
  grid_spec.append([[ppy,ppx]])
  grid_spec.append([])                                           # NO PML   INDECES FOR PROBLEM TYPE (1)
  grid_spec.append([1  ,-1 ])                                    # VIEW REGION INDECES FOR BOTH X AND Y AXES
  grid_spec.append([])                                           # NO TF/SF INDECES FOR PROBLEM TYPE (1)
 else:
  if PARAMS[0][0] in [2]:
   if not PARAMS[1][3]:
    ppx       =int(np.floor((lx*PARAMS[6][0])/dx)) 
    ppy       =int(np.floor((ly*PARAMS[6][1])/dx)) 
    grid_spec.append([[ppy,ppx]])
   else:
    grid_spec.append([])
   grid_spec.append(get_pml_indeces (PARAMS,Nx)              )   # PML   INDECES FOR PROBLEM TYPE (2)
   grid_spec.append([grid_spec[5][0][1]+1,grid_spec[5][1][0]])
   if not PARAMS[1][3]:
    grid_spec.append([])
   else:
    grid_spec.append(get_tfsf_indeces(PARAMS,Nx)               ) # TF/SF INDECES FOR PROBLEM TYPE (2)
 return grid_spec

def get_indeces(PARAMS,GRIDSPEC):
 def get_indeces_Hz(PARAMS,GRIDSPEC):
  indeces_total=[]
  for  j in range(GRIDSPEC[3]-1):
   for i in range(GRIDSPEC[2]-1):
    indeces_total.append((j,i)) 
  if  PARAMS[0][0] in [1]:
   indeces=[indeces_total,[]]
  else:
   if PARAMS[0][0] in [2]:
    indeces_non_pml              =[]
    indeces_pml_vertical_left    =[]
    indeces_pml_vertical_right   =[]
    indeces_pml_horizental_bottom=[]
    indeces_pml_horizental_top   =[]
    for  j in range(GRIDSPEC[5][0][1]+1,GRIDSPEC[5][1][0]  ):
     for i in range(GRIDSPEC[5][0][1]+1,GRIDSPEC[5][1][0]  ):
      indeces_non_pml.append              ((j,i))
    for  j in range(                    GRIDSPEC[3]      -1):
     for i in range(                    GRIDSPEC[5][0][1]+1):
      indeces_pml_vertical_left.append    ((j,i))
    for  j in range(                    GRIDSPEC[3]-1      ):
     for i in range(GRIDSPEC[5][1][0]  ,GRIDSPEC[2]-1      ):
      indeces_pml_vertical_right.append   ((j,i))
    for  j in range(                    GRIDSPEC[5][0][1]+1):
     for i in range(GRIDSPEC[5][0][1]+1,GRIDSPEC[5][1][0]  ):
      indeces_pml_horizental_bottom.append((j,i))
    for  j in range(GRIDSPEC[5][1][0]  ,GRIDSPEC[3]-1      ):
     for i in range(GRIDSPEC[5][0][1]+1,GRIDSPEC[5][1][0]  ):
      indeces_pml_horizental_top.append   ((j,i))
    indeces=[indeces_non_pml                                                                                              ,
             indeces_pml_vertical_left+indeces_pml_vertical_right+indeces_pml_horizental_bottom+indeces_pml_horizental_top ]
  return indeces
 def get_indeces_Ex(PARAMS,GRIDSPEC):
  indeces=[]
  for  j in range(1,GRIDSPEC[3]-1):
   for i in range(  GRIDSPEC[2]-1):
    indeces.append((j,i))
  return indeces
 def get_indeces_Ey(PARAMS,GRIDSPEC):
  indeces=[]
  for  j in range(  GRIDSPEC[3]-1):
   for i in range(1,GRIDSPEC[2]-1):
    indeces.append((j,i))
  return indeces
 indeces_Hz=get_indeces_Hz(PARAMS,GRIDSPEC)
 indeces_Ex=get_indeces_Ex(PARAMS,GRIDSPEC)
 indeces_Ey=get_indeces_Ey(PARAMS,GRIDSPEC)
 return [indeces_Hz,indeces_Ex,indeces_Ey]

# grid_spec[0]: dx
# grid_spec[1]: dt
# grid_spec[2]: Nx
# grid_spec[3]: Ny
# grid_spec[4]: pulse_position
# grid_spec[5]: PML          begin/end indeces
# grid_spec[6]: View  Region begin/end indeces
# grid_spec[7]: TF/SF Region begin/end indeces

# indeces[0][0]: INDECES FOR Hz IN NON-PML REGION
# indeces[0][1]: INDECES FOR Hz IN     PML REGION
# indeces[1]   : INDECES FOR Ex
# indeces[2]   : INDECES FOR Ey


