%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz %%%
%%% GRAPHICS                                    %%%
%%% by: OAMEED NOAKOASTEEN                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% VER      : SIMULATION VERSION
% CONFIG(1): PAUSE
% CONFIG(2): ROTATE IMAGE 90 DEG.                              (false/true)
% CONFIG(3): TYPE OF PLOT: POWER / MAGNITUDE OF ELECTRIC FIELD (P    /Emag)
% CONFIG(4): CLAMP SCALING OF COLORMAP                         (false/true)

function graphics(VER,CONFIG)
clc
close all

PATH                =fullfile('..','..','simulations',VER)                     ;

datapath            =fullfile(PATH,'data')                                     ;
[idx_batch,idx_time]=get_ignore_indeces(PATH)                                  ;
indeces             =rFILENAMES(datapath)                                      ;
CMAX                =get_color_maximum(datapath,indeces,CONFIG{3})             ;

NAME    =fullfile('..','..','simulations',VER,'gif',strcat('data','.gif'))     ;
PAUSE   =CONFIG{1}                                                             ;
h       =figure                                                                ;
counter =idx_batch                                                             ;
for index=idx_batch:size(indeces,2)
    display([' Prossessing Batch ',num2str(counter)])
    filename=fullfile(datapath,strcat('batch_'               ,...
                                      num2str(indeces(index)),...
                                      '.h5'                      ))            ;
    data    =get_data(filename,CONFIG{3})                                      ;
    if index==idx_batch
        ar        =size(data,1)/size(data,2)                                   ;
        fig_config=get(gcf,'position')                                         ;
        set           (gcf,'position',[      fig_config(1)    ,...
                                             fig_config(2)    ,...
                                             fig_config(3)    ,...
                                       floor(fig_config(3)*ar)    ])           ;
    end
    if index==idx_batch
        time_start=idx_time                                                    ;
    else
        time_start=1                                                           ;
    end
    for time=time_start:size(data,3)
        plotter(data(:,:,time),(index-1)*100+time,CONFIG{2},CMAX,CONFIG{4})
        [imind,cm]=rgb2ind(frame2im(getframe(h)),256)                          ;
        if time==time_start && index==idx_batch
            imwrite(imind,cm,NAME,'gif','Loopcount',inf     ,'DelayTime',PAUSE)
        else
            imwrite(imind,cm,NAME,'gif','WriteMode','append','DelayTime',PAUSE)
        end 
    end
    counter=counter+1                                                          ;
end        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function MAXIMUM=get_color_maximum(PATH,INDECES,TYPE)
        MAXIMUM    =[]                                                         ;
        for fidx=1:size(INDECES,2)
            fname  =fullfile(PATH,strcat('batch_',num2str(INDECES(fidx)),'.h5'));
            fdata  =get_data(fname,TYPE)                                       ;
            MAXIMUM=[MAXIMUM,max(max(max(fdata)))]                             ;
        end
        MAXIMUM    =sort(MAXIMUM,'descend')                                    ;
        MAXIMUM    =MAXIMUM(1:floor(size(MAXIMUM,2)/2))                        ;
        MAXIMUM    =min(MAXIMUM)                                               ;
    end

    function [idx_batch,idx_time]=get_ignore_indeces(PATH)
        value    =load(fullfile(PATH,strcat('ignore_period','.csv')))          ;
        idx_batch=floor(value/100)+1                                           ;
        idx_time =mod(value,100)                                               ;
    end

    function INDECES=rFILENAMES(PATH)
        INDECES    =[]                                                         ;
        LIST       =dir(PATH)                                                  ; 
        FILENAMES=string({LIST(4:end).name})                                   ;
        for name=1:size(FILENAMES,2)
            splits =split(FILENAMES{name},'_')                                 ;
            splits =split(splits{2}   ,'.')                                    ;
            INDECES=[INDECES,str2num(splits{1})]                               ;
        end
        INDECES=sort(INDECES)                                                  ;
    end

    function [X,Y]=get_orientation(SWITCH,SIZE)
        if ~ SWITCH
            X=[1:SIZE(2)]                                                      ;
            Y=[1:SIZE(1)]                                                      ;
        else
            X=[1      :   SIZE(1)]                                             ;
            Y=[SIZE(2):-1:1      ]                                             ;
        end
    end

    function returns=get_data(FILENAME,TYPE)
        EX   =h5read (FILENAME,fullfile('/','ex'))                             ;
        EY   =h5read (FILENAME,fullfile('/','ey'))                             ;
        HZ   =h5read (FILENAME,fullfile('/','hz'))                             ;
        EX   =permute(EX,[3,2,1]                 )                             ;
        EY   =permute(EY,[3,2,1]                 )                             ;
        HZ   =permute(HZ,[3,2,1]                 )                             ;
        if strcmp(TYPE,'P')
            returns    =0.5.*abs(HZ).*sqrt((EX).^2+(EY).^2)                    ;
        else
            if strcmp(TYPE,'Emag')
                returns=              sqrt((EX).^2+(EY).^2)                    ;
            end
        end
    end

    function plotter(DATA,TIME,FLIP,CMAX,CLAMPCMAP)
        subplot('Position',[0 0 1 1])
        [ORIENT_X,ORIENT_Y] =get_orientation(FLIP,size(DATA))                  ;
        imagesc(ORIENT_X,ORIENT_Y,DATA)        
        text(0.1*size(DATA,2),0.9*size(DATA,1),...
             num2str(TIME)                    ,...
             'Color'      ,'w'                ,...
             'FontSize'   ,10                 ,...
             'FontWeight' ,'Bold'                  )                           ;
        set(gca,'Visible' ,'off')
        colormap jet
        shading  interp
        axis     equal
        if CLAMPCMAP
            caxis([0 CMAX])
        end
    end      

end