#! /bin/bash

cd run/FDTD1d

echo ' 1D TEz RESONANT CAVITY '

rm     -rf                                   ../../simulations/v51
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v51

echo ' RUNNING SOLVER '
python solver.py -v v51 -pt 1 -p 1 -f 2000 -lx 1 -nt 1100

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v51',{0.05});exit;"


