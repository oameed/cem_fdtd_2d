#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM PEC        WEDGE       CYLINDER '

rm     -rf                                   ../../simulations/v23
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v23

echo ' RUNNING SOLVER '
python solver_PARL.py -v v23 -pt 2 -p 1 -f 2000 -nt 700 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 7

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v23',{0.05,false,'P',false});exit;"


