#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM PEC        CIRCULAR    CYLINDER '

rm     -rf                                   ../../simulations/v21
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v21

echo ' RUNNING SOLVER '
python solver_PARL.py -v v21 -pt 2 -p 1 -f 2000 -nt 700 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 2

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v21',{0.05,false,'P',false});exit;" 


