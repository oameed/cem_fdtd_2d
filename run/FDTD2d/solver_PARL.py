###################################################
### FINITE DIFFERENCE TIME DOMAIN (FDTD) 2D TEz ###
### SOLVER WITH SPLIT-PML ABC                   ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import numpy           as np
import multiprocessing as mp
from paramd import        PATHS, PARAMS
from Gridd  import       (get_grid          ,
                          get_indeces        )
from pulsed import        get_pulse
from CMatd  import        get_coefficients
from auxd   import       (get_AUX           ,
                          interpolate        )
from utilsd import       (wHDF              ,
                          get_milestones    ,
                          get_temp_container,
                          get_temp_index    ,
                          buffer_to_np      ,
                          write_buffer       )

var_dict     ={}
num_cores    =mp.cpu_count()

grid_spec    =get_grid          (PARAMS                       )    

shape_X      =                  (grid_spec[3],grid_spec[2]    )

pulse        =get_pulse         (PARAMS,grid_spec[1],PATHS    )
Cex, Cey, Chz=get_coefficients  (PARAMS,grid_spec             )      # OBJECTS & PML ARE ADDED HERE
indeces      =get_indeces       (PARAMS,grid_spec             )

ex,ey,hz     =get_temp_container(grid_spec,PARAMS[7][0]       )      # TEMPORARY CONTAINERS FOR EXPORT
ViewB,ViewE  =grid_spec[6]                                           # VIEW WINDOW          FOR EXPORT
milestones   =get_milestones    (PARAMS[3][0]                 )      # KEEPING TRACK OF PROGRESS

print(' TIME STEPS   : '+str(PARAMS[3][0])                        ) 
print(' PROBLEM SIZE : '+str(grid_spec[2])+' x '+str(grid_spec[3]))

def init_worker (X0,X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,X11,X12,X13,X14,X15,X16,X17,X18,X19):
 var_dict['ex'   ]=X0
 var_dict['ey'   ]=X1
 var_dict['hz'   ]=X2
 var_dict['hzx'  ]=X3
 var_dict['hzy'  ]=X4
 var_dict['cex0' ]=X5
 var_dict['cex1' ]=X6
 var_dict['cex2' ]=X7
 var_dict['cey0' ]=X8
 var_dict['cey1' ]=X9
 var_dict['cey2' ]=X10
 var_dict['chz0' ]=X11
 var_dict['chz1' ]=X12
 var_dict['chz2' ]=X13
 var_dict['chz3' ]=X14
 var_dict['chz4' ]=X15
 var_dict['chz5' ]=X16
 var_dict['chz6' ]=X17
 var_dict['chz7' ]=X18
 var_dict['shape']=X19

def get_index_flat(ROW,COL):
  return ROW*shape_X[1]+COL

def update_hz_non_pml(INDEX):
 j,i                   = INDEX
 index                 = get_index_flat(j,i)
 var_dict['hz' ][index]=(var_dict['chz0'][index]* var_dict['hz' ][index]                                         +
                         var_dict['chz1'][index]*(var_dict['ex' ][get_index_flat(j+1,i  )]-var_dict['ex'][index])+
                         var_dict['chz2'][index]*(var_dict['ey' ][get_index_flat(j  ,i+1)]-var_dict['ey'][index]) )

def update_hz_pml    (INDEX):
 j,i                   = INDEX
 index                 = get_index_flat(j,i)
 var_dict['hzx'][index]=(var_dict['chz4'][index]* var_dict['hzx'][index]+
                         var_dict['chz5'][index]*(var_dict['ey' ][get_index_flat(j  ,i+1)]-var_dict['ey'][index]) )
 var_dict['hzy'][index]=(var_dict['chz6'][index]* var_dict['hzy'][index]+
                         var_dict['chz7'][index]*(var_dict['ex' ][get_index_flat(j+1,i  )]-var_dict['ex'][index]) )
 var_dict['hz' ][index]= var_dict['hzx' ][index]+ var_dict['hzy'][index]

def update_ex        (INDEX):
 j,i                   = INDEX
 index                 = get_index_flat(j,i)
 var_dict['ex' ][index]=(var_dict['cex0'][index]* var_dict['ex' ][index]+
                         var_dict['cex1'][index]*(var_dict['hz' ][index]-var_dict['hz' ][get_index_flat(j-1,i  )]))

def update_ey        (INDEX):
 j,i                   = INDEX
 index                 = get_index_flat(j,i)
 var_dict['ey' ][index]=(var_dict['cey0'][index]* var_dict['ey' ][index]+
                         var_dict['cey1'][index]*(var_dict['hz' ][index]-var_dict['hz' ][get_index_flat(j  ,i-1)]))

parl_ex      =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_ey      =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_hz0     =mp.RawArray('d', shape_X  [0]*shape_X  [1])            # parl_hz0: Hz
parl_hz1     =mp.RawArray('d', shape_X  [0]*shape_X  [1])            # parl_hz1: Hzx
parl_hz2     =mp.RawArray('d', shape_X  [0]*shape_X  [1])            # parl_hz2: Hzy
parl_cex0    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_cex1    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_cex2    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_cey0    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_cey1    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_cey2    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz0    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz1    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz2    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz3    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz4    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz5    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz6    =mp.RawArray('d', shape_X  [0]*shape_X  [1])
parl_chz7    =mp.RawArray('d', shape_X  [0]*shape_X  [1])

write_buffer(buffer_to_np(parl_cex0,shape_X),Cex[:,:,0])
write_buffer(buffer_to_np(parl_cex1,shape_X),Cex[:,:,1])
write_buffer(buffer_to_np(parl_cex2,shape_X),Cex[:,:,2])
write_buffer(buffer_to_np(parl_cey0,shape_X),Cey[:,:,0])
write_buffer(buffer_to_np(parl_cey1,shape_X),Cey[:,:,1])
write_buffer(buffer_to_np(parl_cey2,shape_X),Cey[:,:,2])
write_buffer(buffer_to_np(parl_chz0,shape_X),Chz[:,:,0])
write_buffer(buffer_to_np(parl_chz1,shape_X),Chz[:,:,1])
write_buffer(buffer_to_np(parl_chz2,shape_X),Chz[:,:,2])
write_buffer(buffer_to_np(parl_chz3,shape_X),Chz[:,:,3])
write_buffer(buffer_to_np(parl_chz4,shape_X),Chz[:,:,4])
write_buffer(buffer_to_np(parl_chz5,shape_X),Chz[:,:,5])
write_buffer(buffer_to_np(parl_chz6,shape_X),Chz[:,:,6])
write_buffer(buffer_to_np(parl_chz7,shape_X),Chz[:,:,7])

init_args=(parl_ex  ,
           parl_ey  ,
           parl_hz0 ,
           parl_hz1 ,
           parl_hz2 ,
           parl_cex0,
           parl_cex1,
           parl_cex2,
           parl_cey0,
           parl_cey1,
           parl_cey2,
           parl_chz0,
           parl_chz1,
           parl_chz2,
           parl_chz3,
           parl_chz4,
           parl_chz5,
           parl_chz6,
           parl_chz7,
           shape_X   )


for NT in range(PARAMS[3][0]):
 
 with mp.Pool(processes=num_cores, initializer=init_worker, initargs=init_args) as pool:
  
  for index in grid_spec[4]:                                         # POINT SOURCE EXCITATION: APPLY MAGNETIC SOURCE DISCONTINUITY
   j,i                 =index
   index_flat          =get_index_flat(j,i)
   parl_hz0[index_flat]=parl_hz0[index_flat]+parl_chz3[index_flat]*pulse[NT]
  
  pool.map(update_hz_non_pml, indeces  [0][0])                       # UPDATE Hz IN NON-PML REGION
  pool.map(update_hz_pml    , indeces  [0][1])                       # UPDATE Hz IN     PML REGION
  pool.map(update_ex        , indeces  [1]   )                       # UPDATE Ex
  pool.map(update_ey        , indeces  [2]   )                       # UPDATE Ey
 
 temp_index        =get_temp_index(NT+1,PARAMS[7][0]) 
 ex[:,:,temp_index]=buffer_to_np(parl_ex ,shape_X)[ViewB:ViewE,ViewB:ViewE]
 ey[:,:,temp_index]=buffer_to_np(parl_ey ,shape_X)[ViewB:ViewE,ViewB:ViewE]
 hz[:,:,temp_index]=buffer_to_np(parl_hz0,shape_X)[ViewB:ViewE,ViewB:ViewE]
 
 if (temp_index+1)==PARAMS[7][0]:
  wHDF(os.path.join(PATHS[1],'batch_'+str(NT)+'.h5'),
       ['ex', 'ey', 'hz'                           ],
       [ ex ,  ey ,  hz                            ] ) 
 
 if NT     in milestones[0]  :
  print(' PROGRESS ... '+str(milestones[1]*10)+' %')
  milestones[1]+=1

print(' FINISHED ')


