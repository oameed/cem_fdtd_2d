#! /bin/bash

cd run/FDTD2d

echo ' 2D TEz SCATTERING FROM DIELECTRIC RECTANGULAR CYLINDER '

rm     -rf                                   ../../simulations/v25
tar    -xzf  ../../simulations/v00.tar.gz -C ../../simulations
mv           ../../simulations/v00           ../../simulations/v25

echo ' RUNNING SOLVER '
python solver_PARL.py -v v25 -pt 2 -p 1 -f 2000 -nt 1000 -ppx 0.394 -ppy 0.606 -ncpw 50 -obj 5

echo ' VISUALIZING '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v25',{0.05,false,'P',false});exit;"


